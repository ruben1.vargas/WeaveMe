//
//  LoomError.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/10/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//

import Foundation

enum LoomError: Int, Error, LocalizedError, CustomStringConvertible {
    case unexplainedFailure = 200
    case invalidResponse = 201
    case notConnected = 202
    case zeroWidthPattern = 203
    case layerCountNotYetEstablished = 204
    case invalidNumberOfResponses = 205
   
    var errorDescription: String? {
        switch self {
        case .unexplainedFailure:
            return "Unexplained failure."
        case .invalidResponse:
            return "Response was not of the correct type."
        case .notConnected:
            return "Bluetooth device is not connected."
        case .zeroWidthPattern:
            return "Zero width patterns can not be transmitted."
        case .layerCountNotYetEstablished:
            return "Number of warp layers not yet established."
        case .invalidNumberOfResponses:
            return "The number of responses was incorrect."
        }
    }
    
    var description: String {
        return "\(errorDescription ?? "Unknown") (\(type(of: self)).\(self.rawValue))"
    }
}
