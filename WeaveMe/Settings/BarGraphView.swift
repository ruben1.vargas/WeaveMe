//
//  BarGraphView.swift
//  WeaveMe
//
//  Created by Kurt Schaefer on 4/20/19.
//  Copyright © 2019 RetroTechJurnal. All rights reserved.
//
// Basic view for plotting a bar graph. You provide the
// values in an array, valueRangeMax will be at the top of the view
// valueRangeMin will be at the bottom of the view, and the values will
// go from left to right with spacing between bars.
//
// This is mostly just used to graph analog values being transmitted from
// the board.

import UIKit

class BarGraphView: UIView {

    var values: [CGFloat] = []

    var valueRangeMin: CGFloat = 0.0  // The value at the bottom of the view
    var valueRangeMax: CGFloat = 1.0  // The value at the top of the view
    var spacing: CGFloat = 0.0    // space in between bars

    var barColor = UIColor.darkGray

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext(),
            !values.isEmpty else {
            return
        }

        var x: CGFloat = 0.0

        let spaceForBars = bounds.size.width - CGFloat(values.count - 1)*spacing
        let barWidth = spaceForBars/CGFloat(values.count)

        context.setFillColor(barColor.cgColor)

        let rangeSize = valueRangeMax - valueRangeMin
        let stepSize = barWidth + spacing

        if rangeSize == 0 {
            return
        }

        for value in values {
            let barHeight = ((value - valueRangeMin)/rangeSize)*bounds.size.height
            let y = bounds.size.height - barHeight
            context.addRect(CGRect(x: x, y: y, width: barWidth, height: barHeight))
            context.fillPath()
            x += stepSize
        }
    }

}
