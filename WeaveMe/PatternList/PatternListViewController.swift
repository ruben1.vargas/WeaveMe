//
//  PatternListViewController.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 8/13/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//

import UIKit

class PatternListViewController: UICollectionViewController, UIDataSourceModelAssociation {

    // TODO:  Right now these patterns hold onto their rendered thumbnail.  That's fine for now since we don't have
    // any way to have a ton of patterns, but this should really have some sort of imageCache (maybe NSCache) that
    // will clear out if the set of images gets too big, etc.
    var patterns: [Pattern] = []
    
    private func buildCropPattern(name: String, primaryImageName: String, edgeImageName: String,
                                  completion: @escaping (Pattern?, Error?) -> Void) {
        let cropLayer = PatternCropLayer()
        let primaryLayer = PatternLayer()
        let edgeLayer = PatternLayer()
        let dispatchGroup = DispatchGroup()
        var errors: [Error] = []
        
        guard let primaryImage = UIImage.init(named: primaryImageName),
            let edgeImage = UIImage.init(named: edgeImageName) else {
                completion(nil, PatternBuildError.imageCouldNotBeLoaded)
                return
        }
        
        dispatchGroup.enter()
        primaryLayer.buildFromImage(primaryImage) { error in
            if let error = error {
                errors.append(error)
            }
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        edgeLayer.buildFromImage(edgeImage) { error in
            if let error = error {
                errors.append(error)
            }
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) {
            if let firstError = errors.first {
                completion(nil, firstError)
                return
            }
            primaryLayer.warpRepeats = 2
            primaryLayer.weftRepeats = 2
            cropLayer.primaryLayer = primaryLayer
            cropLayer.edgeLayer = edgeLayer
            
            let totalWidth = cropLayer.threadCount(.warp)
            let totalHeight = cropLayer.threadCount(.weft)
            // Make sure the edge pattern repeat enough to handle the total size of our pattern including additional edge rows/columns
            edgeLayer.warpRepeats = Int(ceil(Double(totalWidth)/Double(edgeLayer.threadCount(.warp))))
            edgeLayer.weftRepeats = Int(ceil(Double(totalHeight)/Double(edgeLayer.threadCount(.weft))))
            
            let pattern = Pattern()
            pattern.name = name
            pattern.patternLayers = [cropLayer]
            completion(pattern, nil)
        }
    }
    
    private func buildMixPattern(name: String, primaryImageName: String, secondaryImageName: String,
                                 completion: @escaping (Pattern?, Error?) -> Void) {
        let mixLayer = PatternMixingLayer()
        let primaryLayer = PatternLayer()
        let secondaryLayer = PatternLayer()
        let dispatchGroup = DispatchGroup()
        var errors: [Error] = []
        
        guard let primaryImage = UIImage.init(named: primaryImageName),
            let secondaryImage = UIImage.init(named: secondaryImageName) else {
                completion(nil, PatternBuildError.imageCouldNotBeLoaded)
                return
        }
        
        // If this mixing is being done to provide a tabby, it will be a smaller diameter
        secondaryLayer.defaultWeftThreadDiameter = 0.5
        
        dispatchGroup.enter()
        primaryLayer.buildFromImage(primaryImage) { error in
            if let error = error {
                errors.append(error)
            }
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        secondaryLayer.buildFromImage(secondaryImage) { error in
            if let error = error {
                errors.append(error)
            }
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) {
            if let firstError = errors.first {
                completion(nil, firstError)
                return
            }
            
            primaryLayer.weftRepeats = 2
            
            mixLayer.setLayers(primary: primaryLayer, secondary: secondaryLayer)
            
            mixLayer.primaryLayerConsecutiveWefts = 2
            mixLayer.secondaryLayerConsecutiveWefts = 1
            
            let totalWidth = mixLayer.threadCount(.warp)
            let totalHeight = mixLayer.threadCount(.weft)
            // Make sure the edge pattern repeat enough to handle the total size of our pattern including additional edge rows/columns
            secondaryLayer.warpRepeats = Int(ceil(Double(totalWidth)/Double(secondaryLayer.threadCount(.warp))))
            secondaryLayer.weftRepeats = Int(ceil(Double(totalHeight)/Double(secondaryLayer.threadCount(.weft))))
            
            let pattern = Pattern()
            pattern.name = name
            pattern.patternLayers = [mixLayer]
            completion(pattern, nil)
        }
    }

    // Our static list of patterns.
    private var patternDescriptions: [PatternDescription] = [
        PatternDescription(uuid: "b553f0bc-7301-44d0-9157-37b954df0bc2", name: "Plain", imageNames: ["Plain"], warpRepeats: 4, weftRepeats: 4),
        PatternDescription(uuid: "68117f95-fdf8-4265-beed-f329373e529e", name: "Basket 2/2", imageNames: ["Basket_2-2"], warpRepeats: 4, weftRepeats: 4),
        PatternDescription(uuid: "93f5d4a9-f915-4cc0-8bab-6db3bedcd4d9", name: "Rib 1/2", imageNames: ["Rib_1-2"], warpRepeats: 8, weftRepeats: 4),
        PatternDescription(uuid: "3cb61586-8899-46bd-924e-27223443ede2", name: "Rib 2/1", imageNames: ["Rib_2-1"], warpRepeats: 4, weftRepeats: 8),
        PatternDescription(uuid: "bdd8f9e3-af39-4508-a535-eab70da61645", name: "Twill 3/1 Z", imageNames: ["Twill_3-1_Z"], warpRepeats: 4, weftRepeats: 4),
        PatternDescription(uuid: "0bd0fcab-2dd9-425a-94da-88f03cf6589d", name: "Satin5 shift 4", imageNames: ["Satin5_shift_4"], warpRepeats: 3, weftRepeats: 3),
        PatternDescription(uuid: "1f9f8981-be09-4678-97d7-3d69519d9700", name: "Satin8 shift 5", imageNames: ["Satin8_shift_5"], warpRepeats: 2, weftRepeats: 2),
        PatternDescription(uuid: "2a660198-f765-458a-9900-7b3efaea5112", name: "Satin2/6 shift 5", imageNames: ["Satin2-6_shift_5"], warpRepeats: 2, weftRepeats: 2),
        PatternDescription(uuid: "e0711390-2b51-41dd-b61b-9d3c2d4516ca", name: "Warp Color Test", imageNames: ["Warp_Color_Test"], weftRepeats: 4),
        PatternDescription(uuid: "f6cc19ef-1951-4f2f-aed9-dcd85ef39eb2", name: "PandaGround", imageNames: ["PandaGround"]),

        PatternDescription(uuid: "63792a2c-9020-4a75-bc13-3cd83f7d0366", name: "Panda Night",
                           imageNames: ["PandaNightBase", "PandaNightOverlay1", "PandaNightOverlay2"],
                           defaultWarpThreadDiameter: 0.5),
        PatternDescription(uuid: "1a93f028-0d3a-4745-9e24-1436d3da0973", name: "Panda Night (edged)",
                           imageNames: ["PandaNightBase", "PandaNightOverlay1", "PandaNightOverlay2"],
                           defaultWarpThreadDiameter: 0.5,
                           plainWeaveLeftRightEdgeInset: 0),
        PatternDescription(uuid: "02ecadae-ac3a-4a88-8abc-5de2b54f9bc3", name: "Panda Night (edged and mixed)",
                           imageNames: ["PandaNightBase", "PandaNightOverlay1", "PandaNightOverlay2"],
                           defaultWarpThreadDiameter: 0.5,
                           tabbyWeftColorHexString: "#c3bfad",
                           plainWeaveLeftRightEdgeInset: 0),

        PatternDescription(uuid: "02bb59c5-0c80-44fe-84e2-bb9b28794615", name: "Pikachu",
                           imageNames: ["pikachu2Base", "pikachu2Overlay1", "pikachu2Overlay2", "pikachu2Overlay3"],
                           defaultWarpThreadDiameter: 0.5),
        PatternDescription(uuid: "de26f566-740c-42df-99ee-dde95eadf8dc", name: "Pikachu (edged)",
                           imageNames: ["pikachu2Base", "pikachu2Overlay1", "pikachu2Overlay2", "pikachu2Overlay3"],
                           defaultWarpThreadDiameter: 0.5,
                           plainWeaveLeftRightEdgeInset: 4),
        PatternDescription(uuid: "614eda65-07f3-468f-8117-dbb1789d6874", name: "Pikachu (edged and mixed)",
                           imageNames: ["pikachu2Base", "pikachu2Overlay1", "pikachu2Overlay2", "pikachu2Overlay3"],
                           defaultWarpThreadDiameter: 0.5,
                           tabbyWeftColorHexString: "#c3bfad",
                           plainWeaveLeftRightEdgeInset: 4),
    ]
    private func indexOfUUID(_ uuid: String) -> Int {
        if let index = patternDescriptions.firstIndex(where: { $0.uuid == uuid }) {
            return index
        }
        assertionFailure("Looking up the index of a uuid that is not in the list.")
        return 0
    }

    private func buildBasicPatterns(completion: @escaping ([Pattern]) -> Void) {
        let builder = PatternBuilder()

        var newPatterns: [Pattern] = []

        let dispatchGroup = DispatchGroup()

        for patternDescription in patternDescriptions {
            dispatchGroup.enter()
            DispatchQueue.main.async {
                builder.buildPattern(from: patternDescription) { pattern, error in
                    if let pattern = pattern {
                        newPatterns.append(pattern)
                        pattern.buildCache()
                    } else if let error = error {
                        NSLog("Unable to build basic pattern \(patternDescription.name) error: \(error.localizedDescription)")
                    }
                    dispatchGroup.leave()
                }
            }
        }

        dispatchGroup.notify(queue: .main) {
            // The Patterns come in some random order so we sort them to have the same order as in the original list.
            // we sort them and then spruce up their names.
            newPatterns.sort(by: { self.indexOfUUID($0.uuid) < self.indexOfUUID($1.uuid) })
            completion(newPatterns)
        }
    }

    private var isFirstViewWillAppear = true
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if !isFirstViewWillAppear {
            return
        }
        isFirstViewWillAppear = false

        // By now we should know how wide the collection view is, so we update the
        // layout to take up the full width of the display.
        if let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout,
            let collectionSize = collectionView?.bounds.size {
            layout.itemSize = CGSize(width: collectionSize.width, height: layout.itemSize.height)
        }

        buildBasicPatterns() { newPatterns in
            self.patterns = newPatterns
            self.collectionView?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func encodeRestorableState(with coder: NSCoder) {
        super.encodeRestorableState(with: coder)
    }
    
    override func decodeRestorableState(with coder: NSCoder) {
        super.decodeRestorableState(with: coder)
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return patterns.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PatternCell", for: indexPath)
        if let patternCell = cell as? PatternCell {
            patternCell.patternRenderer = PatternRenderer()
            patternCell.pattern = patterns[indexPath.row]
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let patternViewController = PatternViewController.instantiate()
        
        patternViewController.pattern = patterns[indexPath.row]
        navigationController?.pushViewController(patternViewController, animated: true)
    }

    private func restorationIdentifierForPattern(_ pattern: Pattern) -> String {
        return pattern.name + String(describing: type(of: pattern))
    }
    
// MARK: - UIDataSourceModelAssociation
    func modelIdentifierForElement(at idx: IndexPath, in view: UIView) -> String? {
        let pattern = patterns[idx.row]
        return restorationIdentifierForPattern(pattern)
    }
    
    func indexPathForElement(withModelIdentifier identifier: String, in view: UIView) -> IndexPath? {
        for index in 0..<patterns.count {
            let currentIdentifier = restorationIdentifierForPattern(patterns[index])
            if currentIdentifier == identifier {
                return IndexPath(row: index, section: 0)
            }
        }
        return nil
    }
    
}
