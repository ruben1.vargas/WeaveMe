//
//  UIColor+String.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 8/9/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This is from https://gist.github.com/yannickl/16f0ed38f0698d9a8ae7
//  It's pretty lame, and I don't think it handles all color formats, but I just
//  need it to handle RGBA images.

import UIKit

extension UIColor {
    convenience init(hexString: String) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        
        if hexString.hasPrefix("#") {
            scanner.scanLocation = 1
        }
        
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: 1)
    }
    
    func hexString() -> String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb: Int = Int(r*255)<<16 | Int(g*255)<<8 | Int(b*255)<<0
        let alpha: Int = Int(a*255)
        
        var string = String(format: "#%06x", rgb)
        string.append(String(format: "%02x", alpha))

        return string
    }

    // In general I agree with this rule, but for colors it's very clear
    // and not just some abuse.
    // swiftlint:disable large_tuple
    var hsv: (hue: CGFloat, saturation: CGFloat, value: CGFloat) {
        var hue: CGFloat = 0
        var saturation: CGFloat = 0
        var brightness: CGFloat = 0
        var alpha: CGFloat = 0
        
        self.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
        return (hue: hue, saturation: saturation, value: brightness)
    }
    // swiftlint:enable Large Tuple Violation
    
    enum Comparison {
        static let hueAndValue: (UIColor, UIColor) -> Bool = {
            let hsv0 = $0.hsv
            let hsv1 = $1.hsv
            return (hsv0.hue, hsv0.value, hsv0.saturation) < (hsv1.hue, hsv1.value, hsv1.saturation)
        }
    }
}
