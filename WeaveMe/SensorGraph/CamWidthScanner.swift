//
//  CamWidthScanner.swift
//  WeaveMe
//
//  Created by Kurt Schaefer on 4/27/19.
//  Copyright © 2019 RetroTechJurnal. All rights reserved.
//
//  This scanner assume that the loom is set up with both the home and end positions already set,
//  the loom in the home position, and the cams set in alternating positions with the 0th in the forward
//  position.
//
// ge -> Long  Get end limit position.
// gn -> Int   Get number of warp layers.
// gs -> Int   Get averaged sensor value.
// gr -> Int   Get raw sensor value.
// gi -> Int   Get current warp layer index.
// gp -> Long  Get current carriage position.
//
// ch    Mark the current position as carriage home
// ce    Mark the current position as the carriage end.
// cr    Moves the carriage right one warp layer.
// cl    Moves the carriage left one warp layer.
// cn <Long number of warp layers>   Sets the number of warp layers.
// cs <Long threshold> -> <Long position>    Scans carriage toward end looking for rising IR edge.
//                                           A negative threshold seaks for a falling edge.
//
// High == IR Sensor blocked.  Low == IR Sensor is clear.

class CamWidthScanner {
    // Current board seems to go from 677 to 1022, so we use these thresholds.
    let lowThreshold = 710
    let highThreshold = 1000

    let numberOfCams = 60

    // This was determined using a 14 stack of V4 cams
    // With default 4 microsteps.
    let estimagedCamWidth = 304.87

    // In the home state the value will be low because the IR sensor is not yet blocked.

    func scan(completion: @escaping (_ risingEdgePositions: [Int]?, _ fallingEdgePositions: [Int]?) -> Void) {
        var risingEdgePositions: [Int] = []
        var fallingEdgePositions: [Int] = []

        let scanQueue = DispatchQueue(label: "camScanningQueue")
        let orderingGroup = DispatchGroup()
        let requestGroup = DispatchGroup()

        var somethingWentWrong = false

        // There is an extra IR sensor blocker tab that is part of the home position, and
        // since the cams should alternate there are only half as many as the total number of cams.
        // Also the final falling edge may be inacurate and "twice as wide" since the end position
        // also has an extra IR blocking tab.
        for _ in 1...numberOfCams/2 + 1 {
            requestGroup.enter()
            scanQueue.async {

                // Everyone waits on the ordering group so we make these requests sequentially
                orderingGroup.wait()
                orderingGroup.enter()

                if somethingWentWrong {
                    orderingGroup.leave()
                    requestGroup.leave()
                    return
                }

                self.scanForward(to: .risingEdge) { position in
                    guard let position = position else {
                        somethingWentWrong = true
                        orderingGroup.leave()
                        requestGroup.leave()
                        return
                    }
                    NSLog("Got rising edge \(position)")

                    risingEdgePositions.append(position)

                    self.scanForward(to: .fallingEdge) { position in
                        guard let position = position else {
                            somethingWentWrong = true
                            orderingGroup.leave()
                            requestGroup.leave()
                            return
                        }

                        NSLog("Got falling edge \(position)")
                        fallingEdgePositions.append(position)
                        orderingGroup.leave()
                        requestGroup.leave()
                    }
                }
            }
        }

        requestGroup.notify(queue: .main) {
            NSLog("Rising edges \(risingEdgePositions)")
            NSLog("Falling edges \(fallingEdgePositions)")
            completion(risingEdgePositions, fallingEdgePositions)
        }
    }

    enum EdgeType {
        case risingEdge
        case fallingEdge
    }

    private func scanForward(to edgeType: EdgeType, completion: @escaping (_ position: Int?) -> Void) {
        let threshold = edgeType == .risingEdge ? highThreshold : -lowThreshold
        let command = "cs \(threshold) "
        KSBluetoothManager.shared.sendCommand(command, annotation: "Scan carriage forward to IR edge change.",
                                              success: { response in
                                                if let responseString = response {
                                                    completion(Int(responseString))
                                                } else {
                                                    completion(nil)
                                                }
        }, failure: { response in
            completion(nil)
        }, finally: {
        })
    }

}
